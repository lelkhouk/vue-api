import instance from "@/config/axios";

export async function getAuth(form){
    try{
        const response= await instance.post('/auth/local',form);
        console.log(response);
        return response.data;
    }catch(ex){
        console.log(ex)
    }
}

export async function getDogs(){
    try{
        const response= await instance.get('/dogs');
        console.log(response);
        return response.data;
    }catch(ex){
        console.log(ex)
    }
}

export function isAuthenticated(){

    return !!localStorage.getItem('AUTH_TOKEN');
}