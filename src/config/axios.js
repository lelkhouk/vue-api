import axios from "axios";

const instance = axios.create({
    baseURL: 'http://localhost:1337/api'
})

instance.interceptors.request.use(function (config) {

    const authToken = localStorage.getItem('AUTH_TOKEN');
    if (authToken) {
        config.headers.Authorization = `Bearer ${authToken}`;
    }
    return config;
	}, function (error) {
    return Promise.reject(error);
});

export default instance
