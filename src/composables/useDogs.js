
import {getDogs} from '../services/api.services';
import { ref } from 'vue';

export function useDogs(){

    const responseData = ref(null);


    async function fetchDogs() {
        try {
            const response = await getDogs();

            console.log("Réponse Liste dogs:", response);
            responseData.value=response.data;
            console.log('data '+responseData);
            } catch(error) {
                console.error("Erreur lors de la connexion:", error);
            }
    }

    return {
        fetchDogs,
        responseData
    }
}