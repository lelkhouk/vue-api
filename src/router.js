import { createRouter, createWebHashHistory } from 'vue-router'
import Login from "./pages/Login.vue"
import Home from "./pages/Home.vue"
import {isAuthenticated} from './services/api.services'

export const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path:'/login',
            name : 'login',
            component : Login
        },
        {
            path:'/logout',
            name : 'logout'
        },
    ]
});

router.beforeEach((to, from, next) => {
    if (to.name === 'login' && isAuthenticated()) {
        next({ name: 'home' });
    }else if(to.name !== 'login' && !isAuthenticated() ){
        next({name:'login'})
    } else {
        next();
    }
});


